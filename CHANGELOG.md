# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0),
and this project adheres to [Semantic Versioning](https://docs.npmjs.com/about-semantic-versioning).

## [Unreleased]

## [0.4.0] - 2021-09-26

### Added

- `Receiver` method `forEach`
- `Receiver` method `collect`

### Changed

- `Sender` method to accept multiple arguments

## [0.3.0] - 2021-09-26

### Changed

- `Receiver` returned from `channel` to be multi-consumer

## [0.2.0] - 2021-09-25

### Added

- `Sender` class
- `Receiver` class

### Changed

- `Channel` class to factory function `channel`
- `OnceChannel` class to factory function `onceChannel`

## [0.1.0] - 2021-09-25

- Initial multi-producer, single-consumer channel

[unreleased]: https://gitlab.com/rasmusmerzin/mpmc/compare/v0.4.0...master
[0.4.0]: https://gitlab.com/rasmusmerzin/mpmc/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/rasmusmerzin/mpmc/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/rasmusmerzin/mpmc/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/rasmusmerzin/mpmc/tree/v0.1.0
