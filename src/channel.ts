import { Receiver, Sender } from ".";

export function onceChannel<T>(): [(arg: T) => unknown, Promise<T>] {
  let resolve;
  const promise = new Promise<T>((r) => (resolve = r));
  return [resolve as any, promise];
}

export function channel<T>(): [Sender<T>, Receiver<T>] {
  let resolve: undefined | ((arg: T | null) => unknown);
  let promise: undefined | Promise<T | null>;
  [resolve, promise] = onceChannel<T | null>();
  const buffer: Promise<T | null>[] = [promise];
  // help gc
  promise = undefined;

  return [
    new Sender((arg) => {
      if (!resolve) return false;
      let newResolve: undefined | ((arg: T | null) => unknown);
      let newPromise: undefined | Promise<T | null>;
      [newResolve, newPromise] = onceChannel<T | null>();
      buffer.push(newPromise);
      resolve(arg);
      resolve = newResolve;
      // help gc
      newResolve = undefined;
      newPromise = undefined;
      return true;
    }),
    new Receiver(
      async () => {
        if (!buffer.length) return null;
        const promise = buffer[0] as Promise<T | null>;
        const msg = await promise;
        if (buffer[0] === promise) buffer.splice(0, 1);
        return msg;
      },
      () => {
        if (!resolve) return false;
        resolve(null);
        resolve = undefined;
        return true;
      }
    ),
  ];
}
