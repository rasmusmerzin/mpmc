export class Sender<T> {
  send: (...msgs: NonNullable<T>[]) => boolean;

  constructor(send: (msg: NonNullable<T>) => boolean) {
    this.send = (...msgs) => {
      for (const msg of msgs) {
        if (!send(msg)) return false;
      }
      return true;
    };
  }
}
