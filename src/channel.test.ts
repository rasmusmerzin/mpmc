import { Receiver, Sender, channel, onceChannel } from ".";

const A = Symbol();
const B = Symbol();
const C = Symbol();
const D = Symbol();

test("onceChannel", async () => {
  const [resolve, promise] = onceChannel<symbol>();
  setTimeout(resolve, 20, A);
  const start = Date.now();
  expect(await promise).toBe(A);
  expect((Date.now() - start) / 1000).toBeCloseTo(0.02, 2);
});

describe("channel", () => {
  let sender: Sender<symbol>;
  let receiver: Receiver<symbol>;

  beforeEach(() => {
    [sender, receiver] = channel<symbol>();
  });

  test("recv order", async () => {
    sender.send(A);
    setTimeout(() => {
      sender.send(B, C);
      sender.send(D);
    });
    expect(await receiver.recv()).toBe(A);
    expect(await receiver.recv()).toBe(B);
    expect(await receiver.recv()).toBe(C);
    expect(await receiver.recv()).toBe(D);
  });

  test("close", async () => {
    expect(sender.send(A)).toBe(true);
    expect(receiver.close()).toBe(true);
    expect(sender.send(B)).toBe(false);
    expect(receiver.close()).toBe(false);
    expect(await receiver.recv()).toBe(A);
    expect(await receiver.recv()).toBe(null);
    expect(await receiver.recv()).toBe(null);
  });

  test("async iterator", async () => {
    sender.send(A, B, C);
    receiver.close();
    const collected = [];
    for await (const msg of receiver) collected.push(msg);
    expect(collected).toStrictEqual([A, B, C]);
  });

  test("multi-consumer", async () => {
    setTimeout(sender.send, 0, A, B, C);
    setTimeout(receiver.close);
    expect(
      await Promise.all([receiver.recv(), receiver.recv(), receiver.recv()])
    ).toStrictEqual([A, A, A]);
    expect(
      await Promise.all([receiver.recv(), receiver.recv(), receiver.recv()])
    ).toStrictEqual([B, B, B]);
    expect(
      await Promise.all([receiver.recv(), receiver.recv(), receiver.recv()])
    ).toStrictEqual([C, C, C]);
    expect(
      await Promise.all([receiver.recv(), receiver.recv(), receiver.recv()])
    ).toStrictEqual([null, null, null]);
  });

  test("collect", async () => {
    setTimeout(sender.send, 0, A, B);
    setTimeout(receiver.close);
    expect(await receiver.collect()).toStrictEqual([A, B]);
  });
});
