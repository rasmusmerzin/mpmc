export class Receiver<T> {
  recv: () => Promise<T | null>;
  close: () => boolean;

  constructor(recv: () => Promise<T | null>, close: () => boolean) {
    this.recv = recv;
    this.close = close;
  }

  async *[Symbol.asyncIterator](): AsyncGenerator<T> {
    while (true) {
      const msg = await this.recv();
      if (msg == null) return;
      else yield msg;
    }
  }

  forEach = async (f: (msg: T) => unknown): Promise<void> => {
    for await (const msg of this) f(msg);
  };

  collect = async (): Promise<T[]> => {
    const result: T[] = [];
    await this.forEach((msg) => result.push(msg));
    return result;
  };
}
