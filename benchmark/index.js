import { channel } from "mpmc";

const tsBuf = {
  vanilla: [],
  channel: [],
};

let total = 0;
const delays = new Map();

const flushLog = () =>
  Array.from(
    Array(Math.min(tsBuf.vanilla.length, tsBuf.channel.length)),
    () => tsBuf.channel.splice(0, 1)[0] - tsBuf.vanilla.splice(0, 1)[0]
  ).forEach((delay) => {
    total++;
    delays.set(delay, (delays.get(delay) || 0) + 1);
  });

const printLog = () =>
  (document.body.innerText = Array.from(delays.entries())
    .sort(([, a], [, b]) => (a < b ? 1 : -1))
    .map(([delay, count]) => {
      const fDelay = delay.toString().padStart(2, " ");
      const fCount = count.toString().padStart(8, " ");
      const fPercentage = ((100 * count) / total)
        .toFixed(2)
        .toString()
        .padStart(5, " ");
      return `${fDelay}ms ${fCount} ${fPercentage}%`;
    })
    .join("\n"));

const pushLog = (ts, kind) => tsBuf[kind].push(ts);

setInterval(() => {
  flushLog();
  printLog();
}, 33);

document.addEventListener("mousemove", () => pushLog(Date.now(), "vanilla"));
document.addEventListener("keydown", () => pushLog(Date.now(), "vanilla"));

const [sender, receiver] = channel();

document.addEventListener("mousemove", sender.send);
document.addEventListener("keydown", sender.send);
receiver.forEach(() => pushLog(Date.now(), "channel"));
