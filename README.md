# mpmc

[![pipeline](https://img.shields.io/gitlab/pipeline/rasmusmerzin/mpmc/main)](https://gitlab.com/rasmusmerzin/mpmc/-/jobs)
[![coverage](https://img.shields.io/gitlab/coverage/rasmusmerzin/mpmc/main)](https://gitlab.com/rasmusmerzin/mpmc/-/jobs)
[![bundlesize](https://img.shields.io/bundlephobia/minzip/mpmc)](https://bundlephobia.com/package/mpmc)
[![version](https://img.shields.io/npm/v/mpmc)](https://www.npmjs.com/package/mpmc)
[![license](https://img.shields.io/npm/l/mpmc)](https://www.npmjs.com/package/mpmc)
[![downloads](https://img.shields.io/npm/dt/mpmc)](https://www.npmjs.com/package/mpmc)

Async multi-producer, multi-consumer FIFO queue communication utilies.
This is an experimental Typescript library for working with asynchronous streams
of data with a similar API to Rust standard module
[`mpsc`](https://doc.rust-lang.org/std/sync/mpsc/index.html).

## [Benchmark](https://rasmusmerzin.gitlab.io/mpmc)

The core building block of this library is function `onceChannel` which returns
a `Promise` with the `resolve` function.

```javascript
function onceChannel() {
  let resolve;
  const promise = new Promise((r) => (resolve = r));
  return [resolve, promise];
}
```

On top of `onceChannel` is built function `channel` which returns a tuple of
[`Sender`](src/sender.ts) and [`Receiver`](src/receiver.ts).

```typescript
function channel<T>(): [Sender<T>, Receiver<T>];
```

[`Sender`](src/sender.ts) and [`Receiver`](src/receiver.ts) classes loosely
follow the API of Rust
[`Sender`](https://doc.rust-lang.org/std/sync/mpsc/struct.Sender.html) and
[`Receiver`](https://doc.rust-lang.org/std/sync/mpsc/struct.Receiver.html).

```typescript
class Sender<T> {
  constructor(send: (arg: NonNullable<T>) => boolean);

  send(...arg: NonNullable<T>[]): boolean;
}

class Receiver<T> {
  constructor(recv: () => Promise<T | null>, close: () => boolean);

  recv(): Promise<T | null>;
  close(): boolean;
  forEach(f: (arg: T) => unknown): Promise<void>;
  collect(): Promise<T[]>;
  [Symbol.asyncIterator]: AsyncGenerator<T>;
}
```

`Sender.send` method takes non-nullable arguments because `null` is used to
denote that `Receiver` has been closed. `Receiver` also implements
[`asyncIterator`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol/asyncIterator)
protocol to enable it being used in a for loop.

```typescript
for await (const msg of receiver) {
  // ...
}
```

## Extending `Receiver`

It might be useful to extend `Receiver` providing `recv` and `close` methods
and receiving methods `forEach`, `asyncIterator` etc., for free.

For example here is a possible implementation of `WebSocket` extending
`Receiver`.

```typescript
export class Connection extends Receiver<Event> {
  send: (arg: string | ArrayBufferLike | Blob | ArrayBufferView) => boolean;

  constructor(url: string) {
    const socket = new WebSocket(url);
    const [sender, receiver] = channel<Event>();

    socket.onopen = sender.send;
    socket.onerror = sender.send;
    socket.onmessage = sender.send;
    socket.onclose = (event) => {
      sender.send(event);
      receiver.close();
    };

    super(receiver.recv, () => {
      socket.close();
      return receiver.close();
    });

    this.send = (msg) => {
      try {
        socket.send(msg);
        return true;
      } catch (e) {
        return false;
      }
    };
  }
}
```

## Related projects

- [RxJS](https://rxjs.dev) ‒ Reactive Extensions Library for JavaScript
- [Svelte](https://svelte.dev) ‒ Cybernetically enhanced web apps
- [React](https://reactjs.org) ‒ A JavaScript library for building user
  interfaces
